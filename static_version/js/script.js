var defaultValues = [
  {"star":5,"name":"Valeria R","text":"bel viaggio. Pablo è puntuale e socievole. Consigliato vivamente","img":"img.jpg"},
  {"star":1,"name":"Francesca V","text":"Si può far di meglio la prossima volta!","img":"img-2.jpg"}
];

var cApp = angular.module('cacca', []);

cApp.controller('caccaCtrl',['$scope', function functionName($scope) {
  $scope.import = defaultValues;
}]);
