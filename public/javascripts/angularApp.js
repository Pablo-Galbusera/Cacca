var app = angular.module('cacca', ['ui.router']);

app.factory('posts', ['$http', '$interval', '$location', function($http, $interval,$location){
  var o = {
    posts: []
  };

  var automaticPosts = [{"rating":5,"name":"Valeria R.","text":"Testo primo sile","img":"F"},
{"rating":4,"name":"Francesca V.","text":"Quando al figlio Va.","img":"F"},
{"rating":3,"name":"Antonio M","text":"Si va in viaggio per.","img":"M"},
{"rating":2,"name":"Grez F","text":"Allora lui le diessò","img":"M"},
{"rating":1,"name":"Paola C","text":"Perché sì o no?","img":"F"}];

  if ($location.hash()=='superinternet') {

    $interval(loadPosts, 1999);

    function loadPosts () {
      $http.get('/posts').success(function(data){
        angular.copy(data, o.posts);
      });
    }

    o.posts.automaticCounter = 0;

    $interval(saveAutomaticPosts, 10000);

    function saveAutomaticPosts() {
      post = automaticPosts[o.posts.automaticCounter];
      post.time = Date.now();

      if (o.posts.automaticCounter == (automaticPosts.length-1)) {
        o.posts.automaticCounter = 0;
      } else {
        o.posts.automaticCounter++;
      }

      $http.post('/posts', post).success(function(data){
        o.posts.push(data);
      });
    }

  }

  o.getAll = function() {
    return $http.get('/posts').success(function(data){
      angular.copy(data, o.posts);
    });
  };

  o.create = function(post) {
    return $http.post('/posts', post).success(function(data){
      o.posts.push(data);
    });
  };

  o.get = function(id) {
    return $http.get('/posts/' + id).then(function(res){
      return res.data;
    });
  };

  return o;
}]);

app.config([
'$stateProvider',
'$urlRouterProvider',
'$locationProvider',
function($stateProvider, $urlRouterProvider, $locationProvider) {

  $stateProvider
    .state('superinternetloveyou', {
      url: '/superinternetloveyou',
      templateUrl: '/home.html',
      controller: 'MainCtrl',
      // resolve: {
      //   postPromise: ['posts', function(posts){
      //     return posts.getAll();
      //   }]
      // }
    });

  $urlRouterProvider.otherwise('superinternetloveyou');

  //  $locationProvider.html5Mode(true);

}]);

app.controller('MainCtrl', [
'$scope', 'posts', '$interval', '$location',
function($scope, posts, $interval, $location){

if ($location.hash()=='superinternet') {
  $scope.admin = true;
} else {
  $scope.admin = false;
}

$scope.posts = posts.posts;

$scope.addPost = function(){
  if(!this.name || this.name === '') { return; }
  console.log('q');
  posts.create({
    rating: this.rating,
    name: this.name,
    text: this.text,
    img: 'S',
    time: Date.now(),
  });
  this.rating = '';
  this.name = '';
  this.text = '';

  // $scope.posts = posts.posts;

};

$scope.incrementUpvotes = function(post) {
  posts.upvote(post);
};


}]);

app.filter('reverse', function() {
  return function(items) {
    return items.slice().reverse();
  };
});
