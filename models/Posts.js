var mongoose = require('mongoose');

var PostSchema = new mongoose.Schema({
  name: String,
  text: String,
  img: String,
  rating: {type: Number, default: 1},
  time: Number,
});

mongoose.model('Post', PostSchema);
